### phase_1.mk --- 
## 
## Filename: phase_1.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Thu Jul 26 12:53:06 2012 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:

ASSEMBLY_FASTA ?=

PRJ ?= test

CPUS ?= 4

QUERY ?= ../adar_prot.fasta

QUERY_TYPE ?= prot

EVAL ?= 1e-03


assembly.fasta: 
	 ln -sf $(ASSEMBLY_FASTA) $@


ifeq ($(QUERY_TYPE),prot)
query.fasta:
	 ln -sf $(QUERY) $@
else
query.fasta:
	fastatool sanitize $(QUERY) $@
endif




# make DB for BLASTN
assembly.flag: assembly.fasta
	mkdir -p $(basename $<); \
	cd $(basename $<); \
	ln -sf ../$< $(basename $<); \
	makeblastdb -in $(basename $<) -dbtype nucl -title $(basename $<); \
	cd ..; \
	touch $@



ifeq ($(QUERY_TYPE),prot)
define search_DB_tab
	tblastn -evalue $1 -num_threads $$THREADNUM -outfmt "6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qseq sseq" -max_target_seqs 20 -query $2 -db ./$(basename $3)/$(basename $3) -out $4
endef
else
define search_DB_tab
	tblastx -evalue $1 -num_threads $$THREADNUM -outfmt "6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qseq sseq" -max_target_seqs 20 -query $2 -db ./$(basename $3)/$(basename $3) -out $4
endef
endif


ifeq ($(QUERY_TYPE),prot)
define search_DB_pairwise
	tblastn -evalue $1 -num_threads $$THREADNUM -outfmt 0 -max_target_seqs 20 -query $2 -db ./$(basename $3)/$(basename $3) -out $4
endef
else
define search_DB_pairwise
	tblastx -evalue $1 -num_threads $$THREADNUM -outfmt 0 -max_target_seqs 20 -query $2 -db ./$(basename $3)/$(basename $3) -out $4
endef
endif




query.tab: query.fasta assembly.flag
	!threads $(CPUS)
	$(call search_DB_tab,1e-06,$<,$^2,$@)

query.pairwise: query.fasta assembly.flag
	!threads $(CPUS)
	$(call search_DB_pairwise,1e-06,$<,$^2,$@)









.PHONY: test
test: 1e-50_cdna3.tab cdna3_specific.lst
	@echo

# Standard Phony Targets for Users.

# This should be the default target.

# If I have a list of target, I report the list here. 
# For each element of the list, 
# make executes the rule that allows to build it.

# IMPORTANT: You must report all the target files for every
# rule
ALL   += assembly.flag \
	 query.tab \
	 query.pairwise


# The dependency of the targhet listened here
# are treated as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE += 




# Delete all files in the current directory 
# that are normally created by building the program.
CLEAN += assembly \
	 assembly.fasta \
	 assembly.flag \
	 query.fasta \
	 query.tab \
	 query.pairwise



######################################################################
### phase_1.mk ends here
